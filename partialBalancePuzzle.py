'''
    File name: partialBalancePuzzle.py
    Author: Sean Kuehlhorn
    Date created: 7/17/2015
    Date last modified: 7/21/2015
    Python Version: 3.4.3

    This is a variation I made of the well known Counterfeit Coin Balance Puzzle

    Given 9 coins all of which are the same except for one,
    demonstrate the most likely way to find the coin that is different after one comparison.
'''

import random

def twoGroupsOfOne(field):
    if field[0] > field[1]:
        #pick field[0]
        return True
    elif field[0] < field[1]:
        #pick field[1]
        return True
    else:
        #pick between field[2:]
        if random.choice(field[2:]) == 2:
            return True
        else:
            return False

def twoGroupsOfTwo(field):
    if sum(field[:2]) > sum(field[2:4]):
        #pick between field[:2]
        if random.choice(field[:2]) == 2:
            return True
        else:
            return False
    elif sum(field[:2]) < sum(field[2:4]):
        #pick between field [2:4]
        if random.choice(field[2:4]) == 2:
            return True
        else:
            return False
    else:
        #pick between [4:]
        if random.choice(field[4:]) == 2:
            return True
        else:
            return False

def twoGroupsOfThree(field):
    if sum(field[:3]) > sum(field[3:6]):
        # pick betweek field[:3]
        if random.choice(field[:3]) == 2:
            return True
        else:
            return False
    elif sum(field[:3]) < sum(field[3:6]):
        # pick between field[3:6]
        if random.choice(field[3:6]) == 2:
            return True
        else:
            return False
    else:
        # pick between field[6:]
        if random.choice(field[6:]) == 2:
            return True
        else:
            return False

def twoGroupsOfFour(field):
    if sum(field[:4]) > sum(field[4:8]):
        # pick between field[:4]
        if random.choice(field[:4]) == 2:
            return True
        else:
            return False
    elif sum(field[:4]) < sum(field[4:8]):
        # pick between field[4:8]
        if random.choice(field[4:8]) == 2:
            return True
        else:
            return False
    else:
        # pick field[9]
        return True

def makeRandomField():
    x = [2,1,1,1,1,1,1,1,1]
    random.shuffle(x)
    return x

def main():
    correctGuesses = [0,0,0,0]
    cycles = int(input('How many times will the comparison run? ==> '))
    for i in range(cycles):
        f = makeRandomField()
        if twoGroupsOfOne(f):
            correctGuesses[0] += 1
        if twoGroupsOfTwo(f):
            correctGuesses[1] += 1
        if twoGroupsOfThree(f):
            correctGuesses[2] += 1
        if twoGroupsOfFour(f):
            correctGuesses[3] += 1

    print('Two groups of one: {:.5f}'.format(correctGuesses[0]/cycles))
    print('Two groups of two: {:.5f}'.format(correctGuesses[1]/cycles))
    print('Two groups of three: {:.5f}'.format(correctGuesses[2]/cycles))
    print('Two groups of four: {:.5f}'.format(correctGuesses[3]/cycles))

if __name__ == "__main__":
    main()